export interface Event {
  id: Number;
  title: String;
  description_short: String;
  instance: Number;
  start_datetime: Date;
  images: Object;
}
