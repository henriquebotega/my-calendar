import { MaterialModule } from "./../material/material/material.module";
import { SelectorModule } from "./../selector/selector.module";
import { CalendarModule } from "./../calendar/calendar.module";
import { CalendarComponent } from "./../calendar/calendar.component";
import { DashboardComponent } from "./dashboard.component";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, CalendarModule, SelectorModule, MaterialModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DashboardModule {}
