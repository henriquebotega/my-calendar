import { MaterialModule } from "./../material/material/material.module";
import { SelectorComponent } from "./selector.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [SelectorComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  exports: [SelectorComponent],
})
export class SelectorModule {}
