import { MatDatepickerInputEvent } from "@angular/material";
import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-selector",
  templateUrl: "./selector.component.html",
  styleUrls: ["./selector.component.css"],
})
export class SelectorComponent {
  @Output() sendNewDate = new EventEmitter();
  currentDate: Date = new Date();

  constructor() {}

  filterByDate(event: MatDatepickerInputEvent<Date>) {
    this.sendNewDate.emit(event.value);
  }

  previousDay() {
    var yesterday = new Date(
      this.currentDate.setDate(this.currentDate.getDate() - 1)
    );

    this.currentDate = yesterday;
    this.sendNewDate.emit(yesterday);
  }

  nextDay() {
    var tomorrow = new Date(
      this.currentDate.setDate(this.currentDate.getDate() + 1)
    );

    this.currentDate = tomorrow;
    this.sendNewDate.emit(tomorrow);
  }
}
