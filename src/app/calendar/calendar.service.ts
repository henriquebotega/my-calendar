import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Event } from "../interface/event";

@Injectable({
  providedIn: "root",
})
export class CalendarService {
  constructor(private http: HttpClient) {}

  getInfoCalendar() {
    return this.http.post(`https://timelyapp.time.ly/api/calendars/info`, {
      url: "https://calendar.time.ly/ficceyp4",
    });
  }

  getListEventsByDate(
    idCalendar: Number,
    dateSelected: String
  ): Promise<[Event]> {
    return new Promise((resolve) => {
      this.http
        .get(
          `https://timelyapp.time.ly/api/calendars/${idCalendar}/events?start_date=${dateSelected}`
        )
        .subscribe((res) => {
          resolve(res["data"].items);
        });
    });
  }
}
