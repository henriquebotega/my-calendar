import { MaterialModule } from "./../material/material/material.module";
import { CalendarComponent } from "./calendar.component";
import { CalendarService } from "./calendar.service";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [CalendarComponent],
  imports: [CommonModule, HttpClientModule, MaterialModule],
  exports: [CalendarComponent],
  providers: [CalendarService],
})
export class CalendarModule {}
