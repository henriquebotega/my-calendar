import { CalendarService } from "./calendar.service";
import { Component, OnInit, Input } from "@angular/core";
import { Event } from "../interface/event";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.css"],
})
export class CalendarComponent implements OnInit {
  constructor(private calendarService: CalendarService) {}

  items: [Event];
  idCalendar: Number;
  isLoading: Boolean = false;

  private _currentDate = new Date();
  @Input() set currentDate(value) {
    this._currentDate = value;

    if (value) {
      const dtSelected = this.formatDate(value);
      this.getListEvents(dtSelected);
    }
  }
  get currentDate() {
    return this._currentDate;
  }

  ngOnInit() {
    const dtToday = this.formatDate(this.currentDate);

    this.calendarService.getInfoCalendar().subscribe((res: any) => {
      this.idCalendar = res["data"].id;
      this.getListEvents(dtToday);
    });
  }

  getListEvents(dt: string) {
    this.isLoading = true;

    this.calendarService
      .getListEventsByDate(this.idCalendar, dt)
      .then((items: [Event]) => {
        setTimeout(() => {
          this.items = items;
          this.isLoading = false;
        }, 1000);
      });
  }

  formatDate(vlr?: string | Date | null) {
    vlr = !vlr ? new Date().toString() : vlr;
    return new Date(vlr).toISOString().slice(0, 10);
  }

  formatDateToCard(vlr: string) {
    return new Date(vlr).toLocaleDateString("en-US");
  }

  formatImage(image: Object) {
    if (image && image[0].medium) {
      return image[0].medium.url;
    } else {
      return "https://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
  }
}
